
import './App.css';
import Nav from './component/Nav';
import Noteform from './component/NoteForm';
import Notelist from './component/Notelist';
import React, { Component } from 'react';
import {noteData} from './firebaseConect';
import {connect} from 'react-redux';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  // addData = (item) =>{
  //   noteData.push(item)
  // }
  changEditForm =()=>{
    if(this.props. isEditStatus){
      return ( <Noteform/>)
    }
  }
  render() { 
  
    return (
      <div>
      <Nav/>
      <div className="container">
      <div className="row">
        <Notelist/>
        {this.changEditForm()}
         
      </div>
      </div>
    </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    isEditStatus: state.isEdit
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getDataSTT: () => {
      dispatch({type:"EDIT_NOTE"})
    }
  }
}
 export default connect(mapStateToProps, mapDispatchToProps)(App)

