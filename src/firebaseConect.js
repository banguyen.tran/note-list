import firebase from 'firebase';
var firebaseConfig = {
    apiKey: "AIzaSyBSH5C-8MEXeDBwMYtEkvtgDDDN9Na9Bvo",
    authDomain: "note-manage-7f012.firebaseapp.com",
    databaseURL: "https://note-manage-7f012-default-rtdb.firebaseio.com",
    projectId: "note-manage-7f012",
    storageBucket: "note-manage-7f012.appspot.com",
    messagingSenderId: "937682278196",
    appId: "1:937682278196:web:e67a88645d8d2be260b761",
    measurementId: "G-N40G3PFZZ7"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  export const noteData = firebase.database().ref('dataForNote')
  