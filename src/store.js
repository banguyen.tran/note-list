import {noteData} from './firebaseConect';
var redux = require('redux');
const noteInitialState = {
    isEdit: false,
    editObj:{},
    isAdd: false
}
 const allReducer = (state = noteInitialState, action) => {
    switch (action.type) {
        case "ADD_NOTE":
            noteData.push(action.getitem)
         return state
         case "CHANGE_ADD_TITLE":         
         return {...state,  isAdd:!state.isAdd}
            case "EDIT_NOTE":         
         return {...state, isEdit:!state.isEdit}
         case "EDITCONTENT_NOTE":
            return {...state, editObj:action.editdata}
            case "EDIT":
                noteData.child(action.editdata.id).update({
                    title:action.editdata.title,
                    content: action.editdata.content
                })
               return {...state, editObj:{}}
               case "DELETE_NOTE":
                noteData.child(action.deleteData).remove()
               
               return state
        default:
            return state
    }
}
var store = redux.createStore(allReducer)
export default store;