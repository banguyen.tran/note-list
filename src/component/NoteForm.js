import React, { Component } from "react";
import { connect } from "react-redux";
class Noteform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      content: "",
      id: "",
    };
  }
  componentWillMount() {
    if (this.props.editValue) {
      this.setState({
        title: this.props.editValue.title,
        content: this.props.editValue.content,
        id: this.props.editValue.id,
      });
    }
  }
  isChange = (event) => {
    const name = event.target.name;
    console.log('-=--name', name)
    const value = event.target.value;
    this.setState({
      [name]: value,
    });
  };
  addData = (title, content) => {
    if (this.state.id) {
      var editdataObj = {};
      editdataObj.id = this.state.id;
      editdataObj.title = this.state.title;
      editdataObj.content = this.state.content;
      this.props.editData(editdataObj);
      this.props.geteditData();
    } else {
      var item = {};
      item.title = title;
      item.content = content;
      // this.props.getData(item);
      // alert('them du lieu thanh cong')
      this.props.addNote(item);
      this.props.geteditData();
    }
  };
  printTitle = ()=>{
    if(this.props.isAddStt){
      return <h4>Them ghi chu</h4>
    }
    else {
      return <h4>Sua ghi chu</h4>
    }
  }
  
  render() {
    console.log(this.state);
    return (
      <div className="col-4">
       {this.printTitle()}
        <form>
          <div className="form-group">
            <label>Tieu de Note</label>
            <input
              defaultValue={this.props.editValue.title}
              onChange={(event) => this.isChange(event)}
              type="text"
              className="form-control"
              name="title"
              aria-describedby="notetext"
              placeholder="tieu de note"
            />
            <small id="notetext" className="form-text text-muted">
              Dien noi dung Note
            </small>
          </div>
          <div className="form-group">
            <label >Noi dung Note</label>
            <textarea
              onChange={(event) => this.isChange(event)}
              type="text"
              name="content"
              className="form-control"
              aria-describedby="notetext"
              placeholder="Noi dung note"
              defaultValue={this.props.editValue.content}
            />
            <small id="notetext" className="form-text text-muted">
              Dien noi dung Note
            </small>
          </div>
          <button
            type="reset"
            className="btn btn-primary"
            onClick={() => this.addData(this.state.title, this.state.content)}
          >
            Lưu
          </button>
        </form>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    isEditStatus: state.isEdit,
    editValue: state.editObj,
    isAddStt: state.isAdd
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    addNote: (getitem) => {
      dispatch({ type: "ADD_NOTE", getitem });
    },
    geteditData: () => {
      dispatch({ type: "EDIT_NOTE" });
    },
    editData: (editdata) => {
      dispatch({ type: "EDIT", editdata });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Noteform);
