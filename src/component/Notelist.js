import React, { Component } from "react";
import NoteItem from "./NoteItem";
import { noteData } from "../firebaseConect";
class Notelist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firebase: [],
    };
  }

  componentWillMount() {
    noteData.on("value", (item) => {
      var arrayData = [];
      item.forEach((items) => {
        const key = items.key;
        const title = items.val().title;
        const content = items.val().content;
        arrayData.push({
          id: key,
          title: title,
          content: content,
        });
      });
    this.setState({
      firebase: arrayData
    });
    });
  }
  getdata = () => {
    if(this.state.firebase){
   return (this.state.firebase.map((value, key) => {
        return (
          <NoteItem
            key={key}
            contents= {value.content}
            titles={value.title}
            id={key}
            note = {value}
          />
        );
      })
    );
    }
  }
  render() {
    return (
      <div className="col">
        <div id="noteList" role="tablist" aria-multiselectable="true">
          {this.getdata()}
        </div>
      </div>
    );
  }
}

export default Notelist;
