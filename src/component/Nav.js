import React, { Component } from "react";
import { connect } from "react-redux";
class Nav extends Component {
  addNote =(event)=>{
    event.preventDefault();
    this.props.changeNoteForm();
    this.props.changeTitle()
  }
  render() {
    return (
      <nav className="navbar navbar-expand-sm mb-4 navbar-dark bg-dark">
        <a className="navbar-brand" href="#">
          Menu
        </a>
        <button
          className="navbar-toggler d-lg-none"
          type="button"
          data-toggle="collapse"
          data-target="#collapsibleNavId"
          aria-controls="collapsibleNavId"
          aria-expanded="false"
          aria-label="Toggle navigation"
        />
        <div
          className="collapse navbar-collapse d-flex justify-content-end"
          id="collapsibleNavId"
        >
          <ul className="navbar-nav  mt-2">
            <li className="nav-item active">
              <a className="nav-link" href="">
                Home <span className="sr-only">(current)</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="" onClick={(event)=>{this.addNote(event)}}>
                Them moi Note
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    prop: state.prop
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeNoteForm: () => {
      dispatch({type:"EDIT_NOTE"})
    },
    changeTitle: () => {
      dispatch({ type: "CHANGE_ADD_TITLE" });
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Nav)