import React, { Component } from 'react';
import {connect} from "react-redux"
class NoteItem extends Component {
    constructor(props) {
        super(props);
    }
     editDataForm = ()=>{
       this.props.editData()
       //lay du lieu dua vao noteFrom
       this.props.getValueEdit(this.props.note)
     }
     delete = (deleteData)=>{
      this.props.deleteData(this.props.note.id)
      
    }
    render() { 
        return (
            <div className="card">
            <div className="card-header" role="tab" id="note1">
              <h5 className="mb-0">
                <a
                  data-toggle="collapse"
                  data-parent="#noteList"
                  href={"#number"+ this.props.id}
                  aria-expanded="true"
                  aria-controls="note1content"
                >
                  {this.props.titles} <i className="fas fa-wifi-1    " />
                </a>
                <div className="btn-group float-right">
                  <button className="btn btn-outline-info" onClick={()=>this.editDataForm()}>Sua</button>
                  <button className="btn btn-outline-secondary" onClick={()=>this.delete()}>Xoa</button>
                </div>
              </h5>
            </div>
            <div
              id={"number"+this.props.id}
              className="collapse in"
              role="tabpanel"
              aria-labelledby="note1"
            >
              <div className="card-body">
                {this.props.contents}
              </div>
            </div>
          </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
  return {
    isEditState: state.prop
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    editData: () => {
      dispatch({type:"EDIT_NOTE"})
    },
    getValueEdit: (editdata) => {
      dispatch({type:"EDITCONTENT_NOTE",editdata})
  },
  deleteData: (deleteData) => {
    dispatch({ type: "DELETE_NOTE", deleteData });
  }
}
}
 export default connect(mapStateToProps, mapDispatchToProps)(NoteItem)
